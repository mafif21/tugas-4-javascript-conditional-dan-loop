//Soal 1
// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

//jawaban soal 1
var nilai = 54;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else {
  console.log("E");
}

//Soal 2
// buatlah variabel seperti di bawah ini

// var tanggal = 22;
// var bulan = 7;
// var tahun = 2020;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan,
// lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

//Jawaban soal 2
var tanggal = 21;

var bulan = 1;
switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
}

tahun = 2002;

console.log(tanggal + " " + bulan + " " + tahun); //hasilnya = 21 Januari 2002

//soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan
// tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

//jawaban no 3
var angka = 6;
var string = "";
for (var i = 1; i <= angka; i++) {
  for (var j = 0; j < i; j++) {
    string += "#";
  }
  string += "\n";
}
console.log(string);

//soal 4
// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m

//jawaban soal 4
var javascript = "i love Javascript";
var vue = "i love Vue JS";
var prog = "i love Programming";

masukan = 10;
for (i = 1; i <= masukan; i++) {
  if (i % 3 == 1) {
    console.log(i + ". " + prog);
  } else if (i % 3 == 2) {
    console.log(i + ". " + javascript);
  } else {
    console.log(i + ". " + vue);
  }
}
